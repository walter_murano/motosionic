import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'moto',
    loadChildren: () =>
      import('./moto/moto.module').then((m) => m.MotoPageModule),
  },
  {
    path: 'altamotofoto',
    loadChildren: () =>
      import('./altamotofoto/altamotofoto.module').then(
        (m) => m.AltamotofotoPageModule
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
