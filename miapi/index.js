const express = require("express"); 
const cors = require("cors"); 
const bodyParser = require("body-parser");
const app = express();
const multer = require("multer");
const storage = multer.diskStorage({
  destination: "../upload",
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + file.originalname);
  },
});
const upload = multer({ storage: storage });
const baseUrl = "/miapi";
app.use(cors());

app.use(bodyParser.urlencoded({ extended: false })); 
app.use(bodyParser.json());


ddbbConfig = {
  user: "walter-murano-7e3",
  host: "postgresql-walter-murano-7e3.alwaysdata.net",
  database: "walter-murano-7e3_motos",
  password: "cicciabumba",
  port: 5432,
};

const Pool = require("pg").Pool;
const pool = new Pool(ddbbConfig);

const getMotos = (request, response) => {
  var consulta = "SELECT * FROM  motos";
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
    console.log(results.rows);
  });
};
app.get(baseUrl + "/getmotos", getMotos);


const getFiltrarMotos = (request, response) => {
  const marca = capitalize(request.query.marca);

  var consulta = `SELECT * FROM  motos WHERE marca = '${marca}'`;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};
app.get(baseUrl + "/getfiltrarmotos", getFiltrarMotos);



const addMoto = (request, response) => {
  console.log(request.body);
  const { marca, modelo, year, foto, precio } = request.body;
  console.log(marca, modelo, year, foto, precio);
  var consulta = `insert into motos (marca,modelo,year,precio,foto) values ('${marca}','${modelo}','${year}','${precio}','${foto}') `;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};
app.post(baseUrl + "/moto", addMoto);


const deleteMoto = (request, response) => {
  console.log(request.query.id);

  var consulta = `delete FROM motos where id ='${request.query.id}'`;
  pool.query(consulta, (error, results) => {
    if (error) {
      throw error;
    }
    response.status(200).json(results.rows);
  });
};
app.delete(baseUrl + "/delete", deleteMoto);

const PORT = process.env.PORT || 3000; 
const IP = process.env.IP || null; 

app.listen(PORT, IP, () => {
  console.log("El servidor esta inicialitzat en el port " + PORT);
});